<?php
/**
 * icons plugin for Craft CMS 3.x
 *
 * Create dropdown to show icons from md icons or fontawesome
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow
 */

namespace flowsaicons\icons\variables;

use flowsaicons\icons\Icons;

use Craft;

/**
 * icons Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.icons }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Flow
 * @package   Icons
 * @since     1.0.0
 */
class IconsVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.icons.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.icons.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = Icons::$plugin->iconsService->getIcons();
        return $result;;
    }
}
