<?php
/**
 * icons plugin for Craft CMS 3.x
 *
 * Create dropdown to show icons from md icons or fontawesome
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow
 */

/**
 * icons en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('icons', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow
 * @package   Icons
 * @since     1.0.0
 */
return [
    'icons plugin loaded' => 'icons plugin loaded',
];
