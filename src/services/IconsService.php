<?php
/**
 * icons plugin for Craft CMS 3.x
 *
 * Create dropdown to show icons from md icons or fontawesome
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow
 */

namespace flowsaicons\icons\services;

use flowsaicons\icons\Icons;

use Craft;
use craft\base\Component;

/**
 * IconsService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Flow
 * @package   Icons
 * @since     1.0.0
 */
class IconsService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Icons::$plugin->iconsService->exampleService()
     *
     * @return mixed
     */
    public function getIcons()
    {
        function array_delete($array, $element) {
            return (is_array($element)) ? array_values(array_diff($array, $element)) : array_values(array_diff($array, array($element)));
        }
        $icons_file = "font-awesome.css";
        $parsed_file = file_get_contents('https://raw.githubusercontent.com/FortAwesome/Font-Awesome/master/metadata/icons.json');
        $file = json_decode($parsed_file, TRUE);
        echo '<pre>';
        
        var_dump(array_keys($file));
        exit;
        // preg_match_all("/fa\-([a-zA-z0-9\-]+[^\:\.\,\s\{\>])/", $parsed_file, $matches);
        // $exclude_icons = array('fa-pull-left','fa-flip-both','fa-500px','fa-pull-right','fa-pull-left','fa-9x','fa-10x','fa-7x','fa-8x','fa-6x','fa-1x','fa-sm','fa-xs',"fa-lg", "fa-2x", "fa-3x", "fa-4x", "fa-5x", "fa-ul", "fa-li", "fa-fw", "fa-border", "fa-pulse", "fa-rotate-90", "fa-rotate-180", "fa-rotate-270", "fa-spin", "fa-flip-horizontal", "fa-flip-vertical", "fa-stack", "fa-stack-1x", "fa-stack-2x", "fa-inverse");
        // $icons = (object) array("icons" => array_delete($matches[0], $exclude_icons));
    }
}
