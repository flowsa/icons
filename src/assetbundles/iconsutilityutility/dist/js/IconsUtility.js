/**
 * icons plugin for Craft CMS
 *
 * IconsUtility Utility JS
 *
 * @author    Flow
 * @copyright Copyright (c) 2019 Flow
 * @link      www.flowsa.com
 * @package   Icons
 * @since     1.0.0
 */
