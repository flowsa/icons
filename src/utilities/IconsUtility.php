<?php
/**
 * icons plugin for Craft CMS 3.x
 *
 * Create dropdown to show icons from md icons or fontawesome
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow
 */

namespace flowsaicons\icons\utilities;

use flowsaicons\icons\Icons;
use flowsaicons\icons\assetbundles\iconsutilityutility\IconsUtilityUtilityAsset;

use Craft;
use craft\base\Utility;

/**
 * icons Utility
 *
 * Utility is the base class for classes representing Control Panel utilities.
 *
 * https://craftcms.com/docs/plugins/utilities
 *
 * @author    Flow
 * @package   Icons
 * @since     1.0.0
 */
class IconsUtility extends Utility
{
    // Static
    // =========================================================================

    /**
     * Returns the display name of this utility.
     *
     * @return string The display name of this utility.
     */
    public static function displayName(): string
    {
        return Craft::t('icons', 'IconsUtility');
    }

    /**
     * Returns the utility’s unique identifier.
     *
     * The ID should be in `kebab-case`, as it will be visible in the URL (`admin/utilities/the-handle`).
     *
     * @return string
     */
    public static function id(): string
    {
        return 'icons-icons-utility';
    }

    /**
     * Returns the path to the utility's SVG icon.
     *
     * @return string|null The path to the utility SVG icon
     */
    public static function iconPath()
    {
        return Craft::getAlias("@flowsaicons/icons/assetbundles/iconsutilityutility/dist/img/IconsUtility-icon.svg");
    }

    /**
     * Returns the number that should be shown in the utility’s nav item badge.
     *
     * If `0` is returned, no badge will be shown
     *
     * @return int
     */
    public static function badgeCount(): int
    {
        return 0;
    }

    /**
     * Returns the utility's content HTML.
     *
     * @return string
     */
    public static function contentHtml(): string
    {
        Craft::$app->getView()->registerAssetBundle(IconsUtilityUtilityAsset::class);

        $someVar = 'Have a nice day!';
        return Craft::$app->getView()->renderTemplate(
            'icons/_components/utilities/IconsUtility_content',
            [
                'someVar' => $someVar
            ]
        );
    }
}
